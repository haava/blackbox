Source: blackbox
Section: x11
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               libxext-dev,
               libxft-dev,
               libxt-dev
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://sourceforge.net/projects/blackboxwm/
Vcs-Git: https://salsa.debian.org/debian/blackbox.git
Vcs-Browser: https://salsa.debian.org/debian/blackbox

Package: blackbox
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: x-window-manager
Suggests: menu (>=1.5)
Description: Window manager for X
 This is a window manager for X.  It is similar in many respects to
 such popular packages as Window Maker, Enlightenment, and FVWM2.  You
 might be interested in this package if you are tired of window managers
 that are a heavy drain on your system resources, but you still want
 an attractive and modern-looking interface.
 .
 The best part of all is that this program is coded in C++, so it
 is even more attractive "under the hood" than it is in service -- no
 small feat.
 .
 If none of this sounds familiar to you, or you want your computer to
 look like Microsoft Windows or Apple's OS X, you probably don't want
 this package.

Package: libbt-dev
Section: libdevel
Architecture: any
Depends: libbt0 (= ${binary:Version}), libxft-dev, libxt-dev, ${misc:Depends}
Multi-Arch: same
Description: Blackbox - development library
 This is a window manager for X.  It is similar in many respects to
 such popular packages as Window Maker, Enlightenment, and FVWM2.  You
 might be interested in this package if you are tired of window managers
 that are a heavy drain on your system resources, but you still want
 an attractive and modern-looking interface.
 .
 The best part of all is that this program is coded in C++, so it
 is even more attractive "under the hood" than it is in service -- no
 small feat.
 .
 If none of this sounds familiar to you, or you want your computer to
 look like Microsoft Windows or Apple's OS X, you probably don't want
 this package.
 .
 This package contains the development library libbt0

Package: libbt0
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Conflicts: libbt0v5
Replaces: libbt0v5
Provides: libbt0v5
Description: Blackbox - shared library
 This is a window manager for X.  It is similar in many respects to
 such popular packages as Window Maker, Enlightenment, and FVWM2.  You
 might be interested in this package if you are tired of window managers
 that are a heavy drain on your system resources, but you still want
 an attractive and modern-looking interface.
 .
 The best part of all is that this program is coded in C++, so it
 is even more attractive "under the hood" than it is in service -- no
 small feat.
 .
 If none of this sounds familiar to you, or you want your computer to
 look like Microsoft Windows or Apple's OS X, you probably don't want
 this package.
 .
 This package contains the shared library libbt0
